﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace ParseurNombresRomains
{
    class Program
    {
        private static readonly Dictionary<char, int> _chiffresRomain = new Dictionary<char, int>
        {
           {'I', 1}, {'V', 5}, {'X', 10}, {'L', 50}, {'C', 100}, {'D', 500}, {'M', 1000}
        };

        private static readonly List<string> _combinaisonsInterdites = new List<string>
        {
           {"IL"}, {"IC"}, {"ID"}, {"IM"}, {"VX"}, {"VL"}, {"VC"}, {"VD"}, {"VM"}, {"XD"}, {"XM"}, {"LC"}, {"LD"}, {"LM"}, {"DM"}
        };

        static void Main()
        {
            do
            {
                Console.Clear();
                Console.WriteLine("Veuillez entrer un chiffre Romain :");

                string chiffreRomain = Console.ReadLine();

                ParserChiffreRomain(chiffreRomain);

                Console.WriteLine();
                Console.WriteLine("Appuyez sur une touche pour continuer...");
                Console.ReadLine();

            } while (true);
        }
        private static void ParserChiffreRomain(string chiffreRomain)
        {
            // Suppression des espaces
            Regex.Replace(chiffreRomain, @"\s+", "");

            if (EstChiffreRomain(chiffreRomain))
            {
                int chiffreArabe = 0;

                int chiffreCourrant;
                int chiffreSuivant;

                for (int i = 0; i < chiffreRomain.Length; i++)
                {
                    // On commence par récuperer le caractère actuel et le suivant
                    chiffreCourrant = _chiffresRomain[chiffreRomain[i]];

                    if (i + 1 < chiffreRomain.Length)
                    {
                        chiffreSuivant = _chiffresRomain[chiffreRomain[i + 1]];

                        if (chiffreCourrant >= chiffreSuivant)
                        {
                            // Si le caractère suivant est le meme ou est inferieur, il s'additione au caractère actuel
                            chiffreArabe += chiffreCourrant;
                        }
                        else
                        {
                            // Si le caractère suivant est superieur, on doit lui soustraire le caractère actuel
                            chiffreArabe += (chiffreSuivant - chiffreCourrant);

                            // Comme on a déjà utilisé le caractère suivant, on incrémente l'iterateur pour la prochaine boucle
                            i++;
                        }
                    }
                    else
                    {
                        chiffreArabe += chiffreCourrant;
                    }
                }

                Console.WriteLine();
                Console.WriteLine($"Le chiffre Romain [{chiffreRomain}] vaut [{chiffreArabe}] en chiffre Arabe");
            }
        }

        private static bool EstChiffreRomain(string chiffreRomain)
        {
            List<string> erreurs = new List<string>();

            // On vérifie la présence de symboles ne faisant pas partie des chiffres romains
            var symbolesIncorrectes = chiffreRomain.Where(s => !_chiffresRomain.ContainsKey(s)).ToList();

            if (symbolesIncorrectes.Any())
            {
                erreurs.Add($"Les caractères suivant ne sont pas des chiffres romain : {string.Join(",", symbolesIncorrectes)}");
            }

            // On vérifie qu'il n'y a pas de combinaisons de chiffres interdites
            List<string> combinaisonsInterdites = new List<string>();

            foreach (var comb in _combinaisonsInterdites)
            {
                if (chiffreRomain.Contains(comb))
                {
                    combinaisonsInterdites.Add(comb);
                }
            }

            if (combinaisonsInterdites.Any())
            {
                erreurs.Add($"Les combinaisons suivantes ne sont pas autorisées : {string.Join(",", combinaisonsInterdites)}");
            }

            // On vérifie qu'il n'y a pas plus de trois fois un même symbole à la suite
            List<char> symbolesDupliques = new List<char>();

            for (int i = 0; i < chiffreRomain.Length; i++)
            {
                char caractere = chiffreRomain[i];

                var nbRepetition = 1;

                while (i + 1 < chiffreRomain.Length && chiffreRomain[i + 1] == caractere)
                {
                    i++;
                    nbRepetition++;
                }

                if (nbRepetition > 3)
                {
                    symbolesDupliques.Add(caractere);
                }
            }

            if (symbolesDupliques.Any())
            {
                erreurs.Add($"Les symboles suivants ne doivent pas être plus de trois à la suite : {string.Join(",", symbolesDupliques)}");
            }

            // Si il y a au moins une erreur, on ne fait pas la conversion
            if (erreurs.Any())
            {
                Console.WriteLine();
                Console.WriteLine($"Les erreurs suivantes ont été repéré :{Environment.NewLine}" +
                                  $"{string.Join(Environment.NewLine, erreurs)}");

                return false;
            }

            return true;
        }
    }
}
